package main

import (
	"context"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/go-redis/redis"
)

var rdb = redis.NewClient(&redis.Options{
	// keys are set using AWS SSM as env var
	Addr:     os.Getenv("REDIS_URI"),
	Password: os.Getenv("REDIS_SECRETS"),
	DB:       0,
})

type Response events.APIGatewayProxyResponse

func Handler(ctx context.Context, request events.APIGatewayProxyRequest) (Response, error) {
	url := request.QueryStringParameters["short_url"]

	if url == "" {
		return clientError(http.StatusBadRequest, "please provide a url value")
	}

	// check the database for the short URL given
	longURL, err := rdb.Get(url).Result()
	if err != nil {
		return serverError(err)
	}

	// redirect user to the original long url
	return Response{
		StatusCode: http.StatusPermanentRedirect,
		Headers: map[string]string{
			"location": string(longURL),
		},
	}, nil
}

func clientError(status int, err string) (Response, error) {
	return Response{
		StatusCode: status,
		Body:       err,
	}, nil
}

func serverError(err error) (Response, error) {
	return Response{
		StatusCode: http.StatusInternalServerError,
		Body:       http.StatusText(http.StatusInternalServerError),
	}, nil
}

func main() {
	lambda.Start(Handler)
}
