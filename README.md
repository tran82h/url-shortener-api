# url-shortener-api

# Getting Started

The URL shortener api takes longs URLs as input and give shorter ones in return.

A RESTful web service that allows users to POST a URL and receive a short URL in return, or GET a short URL and be redirected to the original long URL.

The below instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

See deployment for notes on how to deploy the project on AWS as a serverless function.

# Prerequisites

- [Go](https://golang.org/) - Go Programming language
- [Docker](https://echo.labstack.com/) - Containers

# Building

```bash
go mod vendor
```

```bash
go build
```

# To deploy to AWS as serverless functions:

### Prerequisites

- Install the Serverless framework to local machine
- Install NPM to local machine
- Have a AWS account with services permission

# Running Locally

## To run using Docker Containers:

Run the following docker-compose command below in your terminal.

_This will build & run both the Redis and the Api in a single container._

```bash
docker-compose up --build
```

# Curl Requests:

To create a short-URL from a provided URL:

```bash
curl --header 'Content-Type: application/json' --request POST --data '{“long_url”: "www.google.com" }' 'http://localhost:1323/url/?long_url=www.google.com'
```

To redirect to the long-URL from a provided short-URL:

```bash
curl -I -X GET 'http://localhost:1323/url/?short_url=http://localhost:1323/bd4030'
```

# To test endpoints on secure live server:

To create a short-URL with a provided URL:

```bash
curl --header 'Content-Type: application/json' --request POST --data '{“long_url”: "www.google.com" }' 'https://code.thetran.info/url/?long_url=www.google.com'
```

To redirect to the long-URL from a provided short-URL:

```bash
curl -I -X GET 'https://code.thetran.info/url/?short_url=https://code.thetran.info/226d64'
```

# Built With

- [Go](https://golang.org/) - Go Programming language
- [AWS](github.com/go-redis/redis) - Cloud Computing Services
- [Echo](https://echo.labstack.com/) - Go Web Framework
- [Docker](https://echo.labstack.com/) - Containers
- [Go-Redis](github.com/go-redis/redis) - Redis Client for Golang
- [Serverless](github.com/go-redis/redis) - Serverless Applications Deployment
