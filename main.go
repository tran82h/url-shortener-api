package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var (
	rdb *redis.Client
)

const (
	slugMaxLen       = 10
	slugMaxRandomInt = 0xffffff
	slugRandomIntFmt = "%06x"
)

var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))
var rndLock sync.Mutex

func rndIntn(n int) int {
	rndLock.Lock()
	n = rnd.Intn(n)
	rndLock.Unlock()
	return n
}

func init() {
	rdb = redis.NewClient(&redis.Options{
		Addr:         "redis:6379",
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
	})
}

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	// Initiate an HTTP Server
	api := echo.New()

	_, err := client.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}

	// Middleware
	api.Use(middleware.Logger())
	api.Use(middleware.Recover())

	// Routes
	api.GET("/url", getUrl)
	api.POST("/url", createUrl)

	// Start api server
	api.Logger.Fatal(api.Start(":1323"))
}

func createUrl(c echo.Context) error {
	scheme := c.Scheme()
	host := c.Request().Host
	url := c.QueryParam("long_url")

	// check and add the scheme to the value if the url
	// value was given without the http prefix
	if !strings.Contains(url, "http") {
		url = "http://" + url
	}

	// check if an empty url value was provided and throw error
	// to user to provide a url value
	if url == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "please provide a valid url")
	}

	location := fmt.Sprintf("%s://%s/%s", scheme, host, generateSlug(url))

	// save the new created short-url to the database
	err := rdb.Set(location, url, 24*time.Hour).Err()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf(""))
	}

	return c.JSON(http.StatusCreated, location)
}

func getUrl(c echo.Context) error {
	url := strings.TrimSpace(c.QueryParam("short_url"))

	// check if an empty url value was given and throw error to user
	// to provide a url value
	if url == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "please provide a url value")
	}

	// check the database for the short URL given
	longURL, err := rdb.Get(url).Result()
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound, fmt.Errorf("url provided not found"))
	}

	// redirect user to the original long url
	return c.Redirect(http.StatusPermanentRedirect, longURL)
}

func generateSlug(source string) (slug string) {
	return generateSlugCustomRandom(source, rndIntn)
}

func generateSlugCustomRandom(source string, intn func(int) int) (slug string) {
	randomSlug := fmt.Sprintf(slugRandomIntFmt, intn(slugMaxRandomInt))

	return randomSlug
}
