package main

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/go-redis/redis"
)

const (
	slugMaxRandomInt = 0xffffff
	slugRandomIntFmt = "%06x"
)

var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))
var rndLock sync.Mutex

func rndIntn(n int) int {
	rndLock.Lock()
	n = rnd.Intn(n)
	rndLock.Unlock()
	return n
}

var rdb = redis.NewClient(&redis.Options{
	// keys are set using AWS SSM as env var
	Addr:     os.Getenv("REDIS_URI"),
	Password: os.Getenv("REDIS_SECRETS"),
	DB:       0,
})

type Response events.APIGatewayProxyResponse

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context, request events.APIGatewayProxyRequest) (Response, error) {
	var err error

	url := request.QueryStringParameters["long_url"]

	// check if an empty url value was given and throw error to user
	// to provide a url value
	if url == "" {
		return clientError(http.StatusBadRequest, "please provide a url value")
	}

	// check and add the scheme to the value if the url
	// value was given without  the http prefix
	if !strings.Contains(url, "http") {
		url = "https://" + url
	}

	// check if the short-url already exist
	// and return it before creating a new one
	shortUrl, err := rdb.Get(url).Result()
	if err == redis.Nil {
	} else if err != nil {
		return serverError(err)
	} else {
		return Response{
			Headers: map[string]string{
				"Access-Control-Allow-Origin": "*",
			},
			StatusCode: http.StatusOK,
			Body:       string([]byte(shortUrl)),
		}, nil
	}

	// create a new short URL with a generated slug
	location := fmt.Sprintf("%s/%s", os.Getenv("DOMAIN_URI"), generateSlug(url))

	// save the new created short-url  to the database
	err = rdb.Set(location, url, 24*time.Hour).Err()
	if err != nil {
		return serverError(err)
	}

	loc, err := json.Marshal(location)
	if err != nil {
		return serverError(err)
	}

	resp := Response{
		StatusCode: http.StatusOK,
		Body:       string(loc),
	}

	return resp, nil
}

func generateSlug(source string) (slug string) {
	return generateSlugCustomRandom(source, rndIntn)
}

func generateSlugCustomRandom(source string, intn func(int) int) (slug string) {
	randomSlug := fmt.Sprintf(slugRandomIntFmt, intn(slugMaxRandomInt))

	return randomSlug
}

func clientError(status int, err string) (Response, error) {
	return Response{
		StatusCode: status,
		Body:       err,
	}, nil
}

func serverError(err error) (Response, error) {
	return Response{
		StatusCode: http.StatusInternalServerError,
		Body:       http.StatusText(http.StatusInternalServerError),
	}, nil
}

func main() {
	lambda.Start(Handler)
}
